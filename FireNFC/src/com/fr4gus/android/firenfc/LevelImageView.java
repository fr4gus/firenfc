package com.fr4gus.android.firenfc;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

/**
 * Custom view to display current game level. Level stacks up from bottom to top.
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */

public class LevelImageView extends View {
    Bitmap mFillBitmap;

    int mMax;

    int mLevel = 0;

    public LevelImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.WatwerSupply);
        int bitmapId = a.getResourceId(R.styleable.WatwerSupply_fillImage, 0);
        mFillBitmap = BitmapFactory.decodeResource(getResources(), bitmapId);
        mMax = a.getInt(R.styleable.WatwerSupply_max, 0);

        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int desiredWidth = widthSize;
        int desiredHeight = heightSize;

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Resize the image to match height based on available space in view and max
        int heighPerLevel = getHeight() / mMax;
        Bitmap levelBmp = mFillBitmap;

        //paint them
        int left = 0;
        int top = getHeight() - (heighPerLevel * mLevel);
        while (top < getHeight()) {
            left = 0;
            canvas.drawBitmap(levelBmp, left, top, null);
            while (left + levelBmp.getWidth() < getWidth()) {
                left += levelBmp.getWidth();
                canvas.drawBitmap(levelBmp, left, top, null);
            }
            top += levelBmp.getHeight();

        }
    }

    public void setLevel(int level) {
        mLevel = level;
    }

    public void setMax(int max) {
        mMax = max;
    }

}
