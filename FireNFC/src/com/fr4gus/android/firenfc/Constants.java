package com.fr4gus.android.firenfc;

/**
 * Interface for global constants
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public interface Constants {
    boolean isDebugMode = true;

    String BROADCAST_GAME_STATUS_UPDATED = "";

    long NEXT_FIRE_MILLIS = 10 * 1000;

    long MINUTE_MILLIS = 60 * 1000;

    long HOUR_MILLIS = 60 * 60 * 1000;

    String PREFERENCES_NAME = "com.fr4gus.android.firenfc.PREFS";

    String PREF_GAMETYPE = "gameType";

    String PREF_SIZE = "size";

    String PREF_QUANTITY = "quantity";

    String FROM_WATER_SUPPLY = "WS";

    String FROM_BUCKET = "B";

    String EXTRA_MAX_QUANTITY = "com.fr4gus.android.firenfc.MAX";

    String EXTRA_TYPE = "com.fr4gus.android.firenfc.TYPE";

    String EXTRA_TITLE = "com.fr4gus.android.firenfc.TITLE";

    String EXTRA_MESSAGE = "com.fr4gus.android.firenfc.MESSAGE";

    String GLOBAL_DIALOG_TAG = "com.fr4gus.android.firenfc.GLOBAL_DIALOG";
    String IGNITE_END_DIALOG_TAG = "com.fr4gus.android.firenfc.IGNITE_END_DIALOG";
    String FINISH_GAME_DIALOG_TAG = "com.fr4gus.android.firenfc.FINISH_GAME_DIALOG";

    // Analytics
    String GA_CATEGORY_UI_ACTION = "ui_action";
}
