package com.fr4gus.android.firenfc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;

/**
 * Due to nature itself of Java and Android Framework, there are several super classes and activity can inherit from (Activity, FragmentActivity, and others)
 * meaning that code that must be shared across different activity types, this class is delegated to execute shared features, across any actitity within the
 * application
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class ActivityDelegate {
    /**
     * Starts an activity (doesn't finish "from" activity) withoy any parameter (bundle)
     * @param from Activity that starts "activityTo"
     * @param activityTo the activity to be launched.
     */
    public static final void startActivity(Activity from, Class<?> activityTo) {
        from.startActivity(new Intent(from, activityTo));
    }

    /**
     * Code to be shared during Activity.onCreate execution
     */
    public static final void onCreate(Activity activity, Bundle bundle) {

    }

    /**
     * Code to be shared during Activity.onStart execution.
     * @param activity activity that executes onStart
     */
    public static final void onStart(Activity activity) {
        EasyTracker.getInstance(activity).activityStart(activity);
    }

    /**
     * Code to be shared during Activity.onStop execution.
     * @param activity activity that executes onStart
     */
    public static final void onStop(Activity activity) {
        EasyTracker.getInstance(activity).activityStop(activity);
    }

    /**
     * Displays a toast message with a Toast.LENGTH_SHORT timeout
     * 
     * @param activity activity where the toast will be displayed
     * @param message string containing the message to be displayed
     */
    public static final void toast(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Displays a toast message with a Toast.LENGTH_SHORT timeout
     * 
     * @param activity activity where the toast will be displayed
     * @param messageId id of the  string resource that represent the message
     */
    public static final void toast(Activity activity, int messageId) {
        Toast.makeText(activity, messageId, Toast.LENGTH_SHORT).show();
    }
}
