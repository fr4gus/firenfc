package com.fr4gus.android.firenfc.util;

import android.util.Log;

/**
 * 
 * Originally from CoreDroid framework https://github.com/tcroft/CoreDroid/
 *
 */
public class LogIt {

    public static void e(Object src, Object... message) {
        e(src, null, message);
    }

    public static void e(Object src, Throwable t, Object... message) {
        StringBuilder builder = new StringBuilder();
        if (t != null) {
            builder.append(t.getMessage()).append(": ");
        } else {
            builder.append("ERROR: ");
        }
        for (Object o : message) {
            builder.append(o).append(", ");
        }

        @SuppressWarnings("rawtypes")
        Class c = src instanceof Class ? (Class) src : src.getClass();
        Log.e(c.getName(), builder.toString(), t);
    }

    public static void d(Object src, Object... message) {
        //		if (!Log.isLoggable(src.getClass().getSimpleName(), Log.DEBUG)) {
        //			return;
        //		}
        StringBuilder builder = new StringBuilder();
        for (Object o : message) {
            builder.append(o).append(", ");
        }
        @SuppressWarnings("rawtypes")
        Class c = src instanceof Class ? (Class) src : src.getClass();
        Log.d(c.getName(), builder.toString());
    }

    public static void w(Object src, Object... message) {
        StringBuilder builder = new StringBuilder();
        for (Object o : message) {
            builder.append(o).append(", ");
        }
        @SuppressWarnings("rawtypes")
        Class c = src instanceof Class ? (Class) src : src.getClass();
        Log.w(c.getName(), builder.toString());

    }

    public static void i(Object src, Object... message) {
        StringBuilder builder = new StringBuilder();
        for (Object o : message) {
            builder.append(o).append(", ");
        }
        @SuppressWarnings("rawtypes")
        Class c = src instanceof Class ? (Class) src : src.getClass();
        Log.i(c.getName(), builder.toString());

    }
}
