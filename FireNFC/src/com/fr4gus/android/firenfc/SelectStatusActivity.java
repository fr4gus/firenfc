package com.fr4gus.android.firenfc;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.RadioGroup;

import com.fr4gus.android.firenfc.game.GameStatus;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

/**
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class SelectStatusActivity extends Activity {
    private RadioGroup mGameTypeGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_status);

        mGameTypeGroup = (RadioGroup) findViewById(R.id.select_game_style);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.select_status, menu);
        return true;
    }

    public void onSelectGameType(View view) {
        int selecterdId = mGameTypeGroup.getCheckedRadioButtonId();
        FireApplication app = FireApplication.getApplication();

        switch (selecterdId) {
        case R.id.select_status_watersupply:
            app.setGameStatus(GameStatus.createWaterSupply(getResources()
                    .getInteger(R.integer.water_supply_size)));
            break;

        case R.id.select_status_bucket:
            app.setGameStatus(GameStatus.createBucket(getResources()
                    .getInteger(R.integer.bucket_size)));
            break;

        default: // fire
            app.setGameStatus(GameStatus.createIgnite(getResources()
                    .getInteger(R.integer.fire_size)));
            break;
        }
        SharedPreferences prefs = getSharedPreferences(
                Constants.PREFERENCES_NAME, MODE_PRIVATE);
        prefs.edit()
                .putInt(Constants.PREF_GAMETYPE,
                        app.getGameStatus().getObjectType().ordinal()).commit();

        EasyTracker easyTracker = EasyTracker.getInstance(this);
        easyTracker.send(MapBuilder.createEvent(
                Constants.GA_CATEGORY_UI_ACTION, "select_game_type",
                app.getGameStatus().getObjectType().toString(), null).build());

        ActivityDelegate.startActivity(this, GameActivity.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ActivityDelegate.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ActivityDelegate.onStop(this);
    }

}
