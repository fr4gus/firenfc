package com.fr4gus.android.firenfc;

import com.fr4gus.android.firenfc.game.GameStatus;
import com.fr4gus.android.firenfc.game.GameStatus.ObjectType;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Displays the actual game status as UI components
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class GameLevelFragment extends Fragment {
    private LevelImageView mGameQuantity;

    private View igniteBackground = null;

    private GameStatus.ObjectType mType;

    public static GameLevelFragment getNewInstance(GameStatus.ObjectType type) {
        GameLevelFragment f = new GameLevelFragment();
        Bundle b = new Bundle();
        b.putSerializable(Constants.EXTRA_TYPE, type);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        Bundle args = getArguments();

        mType = (ObjectType) args.getSerializable(Constants.EXTRA_TYPE);

        int layoutId = 0;
        switch (mType) {
        case WATER_SUPPLY:
            layoutId = R.layout.fragment_water_supply;
            break;
        case BUCKET:
            layoutId = R.layout.fragment_bucket;
            break;
        case IGNITE:
            layoutId = R.layout.fragment_ignite;
            break;
        default:
            //TODO display a layout for unknown state
        }
        View root = inflater.inflate(layoutId, container, false);
        igniteBackground = root;
        mGameQuantity = (LevelImageView) root.findViewById(R.id.WaterLevel);
        return root;
    }

    public void setLevel(int level) {
        mGameQuantity.setLevel(level);
        // If house has burned
        if (level >= mGameQuantity.mMax && mType == ObjectType.IGNITE) {
            ((GameActivity) getActivity()).showIgniteGameOverDialog();
            setIgniteBurned();
            mGameQuantity.setLevel(0);
        }
        mGameQuantity.invalidate();
    }

    private void setIgniteBurned() {
        igniteBackground.setBackgroundResource(R.drawable.bg_burned_house);

    }
}
