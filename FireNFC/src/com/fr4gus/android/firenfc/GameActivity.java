package com.fr4gus.android.firenfc;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.fr4gus.android.firenfc.game.GameStatus;
import com.fr4gus.android.firenfc.game.GameStatus.ObjectType;
import com.fr4gus.android.firenfc.game.QuantityController;
import com.fr4gus.android.firenfc.util.LogIt;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

/**
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class GameActivity extends FragmentActivity implements
        CreateNdefMessageCallback, OnNdefPushCompleteCallback {

    private static final int MESSAGE_START_QUANTITY_CONTROLLER = 100;

    private static final String GAME_FRAGMENT_TAG = "com.fr4gus.android.firenfc.GAME_FRAGMENT";

    private TextView mQuantity;

    private NfcAdapter mNfcAdapter;

    private GameStatus mGameStatus;

    int quantityMoved;

    private BroadcastReceiver mBroadcastReceiver;

    private Handler mQuantityControllerHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MESSAGE_START_QUANTITY_CONTROLLER) {
                Intent intent = new Intent(FireApplication.getApplication(),
                        QuantityController.class);
                startService(intent);

                //Reschedule another call to the quantity service;
                sendEmptyMessageDelayed(MESSAGE_START_QUANTITY_CONTROLLER,
                        getQuantityControllerDelay());
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mQuantity = (TextView) findViewById(R.id.game_quantity);
        mGameStatus = FireApplication.getApplication().getGameStatus();

        if (mGameStatus.getObjectType() != ObjectType.UNKNOWN) {
            // Check for available NFC Adapter
            mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

            if (mNfcAdapter == null) {
                ActivityDelegate.toast(this, R.string.error_nfc_unavailable);
            } else {
                // Register callback to set NDEF message
                mNfcAdapter.setNdefPushMessageCallback(this, this);

                // Register callback to listen for message-sent success
                mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
            }

            // Puts game fragment 
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(
                            R.id.FragmentContainer,
                            GameLevelFragment.getNewInstance(mGameStatus
                                    .getObjectType()), GAME_FRAGMENT_TAG)
                    .commit();
        } else {
            ActivityDelegate.startActivity(this, SelectStatusActivity.class);
            finish();
        }
    }

    private void updateUi() {
        if (mGameStatus.getObjectType() == ObjectType.UNKNOWN) {
            return;
        }
        mQuantity.setText(String.valueOf(mGameStatus.getQuantity()));

        GameLevelFragment f = getGameLevelView();
        if (f != null) {
            f.setLevel(mGameStatus.getQuantity());
        } else {
            LogIt.d(this, "Fragment not set yet");
        }

    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {

        // Data will be form of <sender_type> <quantity>.
        // for examples a water supply always sends "WS 1", one unit at a time
        // bucket in other hand, sends all its size "B 1" for a bucket of 1 unit
        // ignite doesn't send units

        String data = "";
        switch (mGameStatus.getObjectType()) {
        case WATER_SUPPLY:
            if (mGameStatus.getQuantity() > 0) {
                data = "WS 1";
                // Reduce one quantity of water
                quantityMoved = 1;
            } else {
                LogIt.d(this, "Run out of water");
            }
            break;
        case BUCKET:
            if (mGameStatus.getQuantity() > 0) {
                // When "using" a bucket, it is to use the whole water within
                data = "B " + String.valueOf(mGameStatus.getQuantity());
                quantityMoved = mGameStatus.getSize();
            } else {
                LogIt.d(this, "Run out of water");
            }

            break;
        default:
            // do nothing, ignite can't send messages
        }
        LogIt.d(this, "Sending the following amount of water", data);
        NdefMessage msg = null;
        byte[] payload = data.getBytes();

        NdefRecord cardRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                NdefRecord.RTD_TEXT, new byte[0], payload);

        msg = new NdefMessage(new NdefRecord[] { cardRecord });
        return msg;
    }

    @Override
    public void onNdefPushComplete(NfcEvent arg0) {

        mGameStatus.setQuantity(mGameStatus.getQuantity() - quantityMoved);
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                updateUi();

            }
        });
    }

    @Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            processIntent(intent);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mBroadcastReceiver);

        disableQuantityController();

        FireApplication.getApplication().saveGameStatus(mGameStatus);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUi();
        mBroadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                LogIt.d(this, "GameStatus modified, updating UI");

                updateUi();

            }
        };
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                .getInstance(this);
        localBroadcastManager.registerReceiver(mBroadcastReceiver,
                new IntentFilter(Constants.BROADCAST_GAME_STATUS_UPDATED));

        // When a new intent is received, the activity is sent to pause, then the lifecycle will invoke again
        // onResome, and IntentService must be called again. When on processIntent method, it will be decided
        // if intent service must be shut down again.
        if (quantityControllerRequired()) {
            LogIt.d(this, "Quantity Controller will be initiated every ",
                    getQuantityControllerDelay(), "milliseconds");
            enableQuantityController(getQuantityControllerDelay(),
                    getQuantityControllerDelay());
        }
        // In case we return from an ending game, don't keep incrementing content
        if (mGameStatus.getObjectType() == ObjectType.IGNITE
                && mGameStatus.getQuantity() == mGameStatus.getSize()) {
            disableQuantityController();
        }

    }

    /**
     * Parses the NDEF Message from the intent and updates the UI to match the new state
     */
    void processIntent(Intent intent) {

        Parcelable[] rawMsgs = intent
                .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam

        if (rawMsgs != null) {
            NdefMessage msg = (NdefMessage) rawMsgs[0];
            // record 0 contains the MIME type, record 1 is the AAR, if present
            String[] data = new String(msg.getRecords()[0].getPayload())
                    .split(" ");

            // If not enough parameters, ignore message. Message should be of the form <source> <quantity>
            if (data.length < 2)
                return;

            String from = data[0];
            int quantityReceived = Integer.valueOf(data[1]);

            LogIt.d(this, "Recieved the following quantity of water:",
                    quantityReceived);
            switch (mGameStatus.getObjectType()) {
            case BUCKET:
                if (Constants.FROM_WATER_SUPPLY.equals(from)
                        || Constants.FROM_BUCKET.equals(from)) {
                    if (mGameStatus.getQuantity() < mGameStatus.getSize()) {
                        mGameStatus.setQuantity(mGameStatus.getQuantity()
                                + quantityReceived);
                    } else {
                        ActivityDelegate.toast(this,
                                R.string.error_too_much_water);
                    }
                } else {
                    LogIt.i(this, "Received something from ", from);
                }
                break;
            case IGNITE:
                // This is going to delay the next fire 
                if (Constants.FROM_WATER_SUPPLY.equals(from)) {
                    // Water that comes from Bucket is applied with 60% effectiveness
                    int random = (int) (Math.random() * 10);
                    if (random < 6) {
                        mGameStatus.setQuantity(mGameStatus.getQuantity()
                                - quantityReceived);
                        disableQuantityController();
                        enableQuantityController(Constants.MINUTE_MILLIS,
                                getQuantityControllerDelay());
                    } else {
                        ActivityDelegate.toast(this,
                                R.string.error_ignite_from_water_supply);
                    }
                } else if (Constants.FROM_BUCKET.equals(from)) {
                    // Water that comes from Bucket is applied with 100% effectiveness 
                    mGameStatus.setQuantity(mGameStatus.getQuantity()
                            - quantityReceived);
                    disableQuantityController();
                    enableQuantityController(Constants.MINUTE_MILLIS,
                            getQuantityControllerDelay());
                } else {
                    LogIt.i(this, "Received something from ", from);
                }
                break;
            default:
                // WatterSupply won't receive water by NFC
                LogIt.i(this, "Water supply can not receive water from ", from);
                break;
            }
            updateUi();
        } else {
            ActivityDelegate.toast(this, R.string.error_empty_beam);
        }
    }

    public void restart(View view) {
        EasyTracker easyTracker = EasyTracker.getInstance(this);
        easyTracker.send(MapBuilder.createEvent(
                Constants.GA_CATEGORY_UI_ACTION, "restart_game", null, null)
                .build());

        ActivityDelegate.startActivity(this, SelectStatusActivity.class);
        finish();
    }

    private GameLevelFragment getGameLevelView() {
        return (GameLevelFragment) getSupportFragmentManager()
                .findFragmentByTag(GAME_FRAGMENT_TAG);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ActivityDelegate.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ActivityDelegate.onStop(this);
    }

    private void enableQuantityController(long delay, long intervalMillis) {
        LogIt.d(this, "Enabling Quantity Controller, to start at", new Date(
                System.currentTimeMillis() + delay), "every", intervalMillis);

        mQuantityControllerHandler.sendEmptyMessageDelayed(
                MESSAGE_START_QUANTITY_CONTROLLER, delay);
    }

    private void disableQuantityController() {
        LogIt.d(this, "Disabling Quantity Controller");
        mQuantityControllerHandler
                .removeMessages(MESSAGE_START_QUANTITY_CONTROLLER);

    }

    private long getQuantityControllerDelay() {
        switch (mGameStatus.getObjectType()) {
        case WATER_SUPPLY:
            return Constants.MINUTE_MILLIS;
        case IGNITE:
            return getResources().getInteger(R.integer.fire_speed);
        default:
            return 0;
        }
    }

    private boolean quantityControllerRequired() {
        switch (mGameStatus.getObjectType()) {
        case WATER_SUPPLY:
        case IGNITE:
            return true;
        default:
            return false;
        }

    }

    public void doPositiveClick(String dialogTag) {
        if (Constants.IGNITE_END_DIALOG_TAG.equals(dialogTag)) {
            EasyTracker easyTracker = EasyTracker.getInstance(this);
            easyTracker.send(MapBuilder.createEvent(
                    Constants.GA_CATEGORY_UI_ACTION, "restart_game_over", null,
                    null).build());

        } else if (Constants.FINISH_GAME_DIALOG_TAG.equals(dialogTag)) {
            EasyTracker easyTracker = EasyTracker.getInstance(this);
            easyTracker.send(MapBuilder.createEvent(
                    Constants.GA_CATEGORY_UI_ACTION, "finish_game", null,
                    null).build());
        }
        ActivityDelegate.startActivity(this, SelectStatusActivity.class);
        finish();

    }

    public void doNegativeClick(String dialogTag) {
        if (Constants.IGNITE_END_DIALOG_TAG.equals(dialogTag)) {
            EasyTracker easyTracker = EasyTracker.getInstance(this);
            easyTracker.send(MapBuilder
                    .createEvent(Constants.GA_CATEGORY_UI_ACTION,
                            "dont_restart", null, null).build());

            mGameStatus = GameStatus.createUknown();
            FireApplication.getApplication().setGameStatus(mGameStatus);
            finish();
        } else if (Constants.FINISH_GAME_DIALOG_TAG.equals(dialogTag)) {
            EasyTracker easyTracker = EasyTracker.getInstance(this);
            easyTracker.send(MapBuilder
                    .createEvent(Constants.GA_CATEGORY_UI_ACTION,
                            "keep_playing", null, null).build());

            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            Fragment f = manager.findFragmentByTag(dialogTag);
            if ( f != null){
                ft.remove(f);
            }
            ft.commit();
        }
    }

    void showIgniteGameOverDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(
                Constants.IGNITE_END_DIALOG_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        disableQuantityController();
        // Create and show the dialog.
        DialogFragment newFragment = YesNoAlertDialog.newInstance(
                R.string.ignite_game_over_title, R.string.ignite_game_over,
                Constants.FINISH_GAME_DIALOG_TAG);
        newFragment.show(ft, Constants.IGNITE_END_DIALOG_TAG);
    }

    @Override
    public void onBackPressed() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(
                Constants.FINISH_GAME_DIALOG_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment newFragment = YesNoAlertDialog.newInstance(
                R.string.exit_game_title, R.string.exit_game_message,
                Constants.FINISH_GAME_DIALOG_TAG);
        newFragment.show(ft, Constants.FINISH_GAME_DIALOG_TAG);

    }

}
