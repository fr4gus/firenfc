package com.fr4gus.android.firenfc.game;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import com.fr4gus.android.firenfc.Constants;
import com.fr4gus.android.firenfc.FireApplication;
import com.fr4gus.android.firenfc.game.GameStatus.ObjectType;
import com.fr4gus.android.firenfc.util.LogIt;

/**
 * Only Water Supply or Ignite should invoke this intent service. The idea is that a water supply can
 * "regenerate" water every hour. A Ignite in the other hand has less luck, and fire gets checked every minute
 * and has certain probability of increasing the fire or note (80/20).
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class QuantityController extends IntentService {

    public QuantityController() {
        super("QuantityController");
    }

    @Override
    protected void onHandleIntent(Intent arg0) {
        LogIt.d(this, "Quantity Controller started");
        GameStatus gameStatus = FireApplication.getApplication()
                .getGameStatus();

        if (gameStatus.getObjectType().equals(ObjectType.IGNITE)) {

            int prob = (int) (Math.random() * 10.0);
            if (prob > 2) {
                LogIt.d(this, "Quantity kept");
                // no fire is added
            } else {
                // quantity increased
                LogIt.d(this, "Quantity increased");
                gameStatus.setQuantity(gameStatus.getQuantity() + 1);
            }
        } else if (gameStatus.getObjectType().equals(ObjectType.WATER_SUPPLY)) {
            gameStatus.setQuantity(gameStatus.getQuantity() + 1);
        }

        SharedPreferences prefs = getSharedPreferences(
                Constants.PREFERENCES_NAME, MODE_PRIVATE);
        prefs.edit().putInt(Constants.PREF_QUANTITY, gameStatus.getQuantity())
                .commit();

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                .getInstance(this);
        localBroadcastManager.sendBroadcast(new Intent(
                Constants.BROADCAST_GAME_STATUS_UPDATED));
    }

}
