package com.fr4gus.android.firenfc.game;

/**
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class GameStatus {
    public static enum ObjectType {
        WATER_SUPPLY, BUCKET, IGNITE, UNKNOWN
    }

    /**
     * Water Supply from 0 to N (no defined). Since its a "huge" container it
     * can hold as much as possible (for now) Ideally we can define a maximum
     * and user can upgrade to bigger container. What it contains is water
     * 
     * Bucket fom 0 to 1 (for now). User my, later, upgrade its bucket size.
     * Never as big as a water supply.
     * 
     * Ignite. from N to 0. Depending of the type of ignite (paper, box, house,
     * building, etc)
     */
    private int quantity;

    /**
     * The maxium of quantity this "container" can hold
     */
    private int size;

    private ObjectType objectType;

    private GameStatus(ObjectType type) {
        objectType = type;
    }

    public final int getQuantity() {
        return quantity;
    }

    public final void setQuantity(int quantity) {
        if (size < 0) {
            throw new IllegalArgumentException("Size can't be zero or negative");
        }
        if (quantity <= size) {
            if (quantity < 0) {
                quantity = 0;
            }
            this.quantity = quantity;
        } else {
            this.quantity = size;
        }
    }

    public final int getSize() {
        return size;
    }

    public final void setSize(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size can't be zero or negative");
        }
        this.size = size;
    }

    public final ObjectType getObjectType() {
        return objectType;
    }

    private static final GameStatus createGameStatus(ObjectType type, int size,
            int quantity) {
        GameStatus status = new GameStatus(type);

        status.setSize(size);
        status.setQuantity(quantity);
        return status;
    }

    public static final GameStatus createWaterSupply(int size) {
        return createGameStatus(ObjectType.WATER_SUPPLY, size, size);
    }

    public static final GameStatus createBucket(int size) {
        return createGameStatus(ObjectType.BUCKET, size, 0);
    }

    public static final GameStatus createIgnite(int size) {
        return createGameStatus(ObjectType.IGNITE, size, 0);
    }

    public static final GameStatus createUknown() {
        return new GameStatus(ObjectType.UNKNOWN);
    }
}
