package com.fr4gus.android.firenfc;

import android.app.Activity;
import android.os.Bundle;

import com.fr4gus.android.firenfc.game.GameStatus;
import com.fr4gus.android.firenfc.game.GameStatus.ObjectType;
import com.fr4gus.android.firenfc.util.BackgroundTask;

/**
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class LauncherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

    }

    @Override
    protected void onResume() {
        super.onResume();

        new BackgroundTask() {

            @Override
            public void work() {
                try {
                    Thread.sleep(2000);
                    FireApplication.getApplication().loadGameStatus();
                } catch (InterruptedException ignored) {
                }
            }

            @Override
            public void done() {
                GameStatus state = FireApplication.getApplication()
                        .getGameStatus();
                if (state.getObjectType() == ObjectType.UNKNOWN) {
                    ActivityDelegate.startActivity(LauncherActivity.this,
                            SelectStatusActivity.class);
                } else {
                    ActivityDelegate.startActivity(LauncherActivity.this,
                            GameActivity.class);
                }
                finish();
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        ActivityDelegate.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ActivityDelegate.onStop(this);
    }

}
