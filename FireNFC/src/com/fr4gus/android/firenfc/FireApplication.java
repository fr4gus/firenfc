package com.fr4gus.android.firenfc;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.fr4gus.android.firenfc.game.GameStatus;
import com.fr4gus.android.firenfc.game.GameStatus.ObjectType;

/**
 * 
 * @author Franklin Garcia me@fr4gus.com
 *
 */
public class FireApplication extends Application {
    private GameStatus mGameStatus;

    private static FireApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public final GameStatus getGameStatus() {
        return mGameStatus;
    }

    public final GameStatus loadGameStatus() {
        SharedPreferences prefs = getSharedPreferences(
                Constants.PREFERENCES_NAME, MODE_PRIVATE);
        int t = prefs.getInt(Constants.PREF_GAMETYPE, -1);
        int size = prefs.getInt(Constants.PREF_SIZE, 0);
        int quantity = prefs.getInt(Constants.PREF_QUANTITY, 0);
        if (t != -1) {
            ObjectType type = ObjectType.values()[t];
            switch (type) {
            case WATER_SUPPLY:
                if (size == 0)
                    size = getResources().getInteger(
                            R.integer.water_supply_size);
                mGameStatus = GameStatus.createWaterSupply(size);
                break;

            case BUCKET:
                if (size == 0)
                    size = getResources().getInteger(R.integer.bucket_size);
                mGameStatus = GameStatus.createBucket(size);
                break;
            default:
                if (size == 0)
                    size = getResources().getInteger(R.integer.fire_size);
                mGameStatus = GameStatus.createIgnite(size);
                break;
            }
            mGameStatus.setQuantity(quantity);
        } else {
            mGameStatus = GameStatus.createUknown();
        }

        return mGameStatus;
    }

    public final void saveGameStatus(GameStatus status) {
        SharedPreferences prefs = getSharedPreferences(
                Constants.PREFERENCES_NAME, MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putInt(Constants.PREF_GAMETYPE, mGameStatus.getObjectType()
                .ordinal());
        editor.putInt(Constants.PREF_QUANTITY, mGameStatus.getQuantity());
        editor.putInt(Constants.PREF_SIZE, mGameStatus.getSize());
        editor.commit();

    }

    public final void setGameStatus(GameStatus mGameStatus) {
        this.mGameStatus = mGameStatus;
    }

    public static final FireApplication getApplication() {
        return mInstance;
    }

}
