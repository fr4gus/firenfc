package com.fr4gus.android.firenfc;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class YesNoAlertDialog extends DialogFragment {
    public static YesNoAlertDialog newInstance(int title, int message,
            String tag) {
        YesNoAlertDialog frag = new YesNoAlertDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putInt("message", message);
        args.putString("tag", tag);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int title = getArguments().getInt("title");
        int message = getArguments().getInt("message");
        final String tag = getArguments().getString("tag");
        return new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.prompt_yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                ((GameActivity) getActivity())
                                        .doPositiveClick(tag);
                            }
                        })
                .setNegativeButton(R.string.prompt_no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                    int whichButton) {
                                ((GameActivity) getActivity())
                                        .doNegativeClick(tag);
                            }
                        }).create();
    }
}
